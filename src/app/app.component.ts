import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { AddComponent } from './component/add/add.component';
import { Board } from './interfaces';
import { BoardService } from './service/board.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  boards: Board[];

  constructor(public dialog: MatDialog, private boardService: BoardService) {}

  getBoards(): void {
    this.boardService.getBoards().subscribe((boards) => {
      this.boards = boards;
    });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(AddComponent, {
      data: { text: '' },
    });

    dialogRef.afterClosed().subscribe((result: any) => {
      if (result) {
        this.boardService.addBoard(result);
      }
    });
  }

  ngOnInit(): void {
    this.getBoards();
  }
}

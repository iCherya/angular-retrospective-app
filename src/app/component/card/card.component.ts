import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { Card, Comment } from '../../interfaces';
import { CardService } from '../../service/card.service';
import { CommentService } from '../../service/comment.service';
import { AddComponent } from '../add/add.component';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent implements OnInit {
  @Input() cardId: number;

  card: Card;
  comments: Comment[];

  constructor(
    public dialog: MatDialog,
    private cardService: CardService,
    private commentService: CommentService
  ) {}

  getData(): void {
    this.commentService
      .getComments(this.cardId)
      .subscribe((comments) => (this.comments = comments));

    this.cardService
      .getCardById(this.cardId)
      .subscribe((card) => (this.card = card));
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(AddComponent, {
      data: { text: '' },
    });

    dialogRef.afterClosed().subscribe((result: any) => {
      if (result) {
        this.commentService.addComment(this.cardId, result);
        this.getData();
      }
    });
  }

  updateRating(value: number): void {
    this.cardService.updateCardRating(this.cardId, value);
    this.getData();
  }

  ngOnInit(): void {
    this.getData();
  }
}

export interface Board {
  id: number;
  text: string;
}

export interface Card {
  id: number;
  boardId: number;
  text: string;
  rating: number;
}

export interface Comment {
  id: number;
  cardId: number;
  text: string;
}

export interface Database {
  boards: Board[];
  cards: Card[];
  comments: Comment[];
}

import { Injectable } from '@angular/core';

import { Database } from './../interfaces';
import { DATABASE } from './../mock/database.mock';

@Injectable({
  providedIn: 'root',
})
export class DatabaseService {
  private state: Database = { comments: [], cards: [], boards: [] };

  constructor() {
    if (localStorage.getItem('database')) {
      const database = JSON.parse(localStorage.getItem('database') || '');
      this.state = { ...database };
    } else {
      this.state = { ...DATABASE };
    }
  }

  add(field: 'boards' | 'cards' | 'comments', item: any): void {
    this.state[field].push(item);

    this.save();
  }

  save(): void {
    localStorage.setItem('database', JSON.stringify(this.state));
  }

  get(): Database {
    return this.state;
  }
}

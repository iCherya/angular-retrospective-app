import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { Comment } from '../interfaces';

import { DatabaseService } from './database.service';

@Injectable({
  providedIn: 'root',
})
export class CommentService {
  COMMENTS: Comment[];

  constructor(private databaseService: DatabaseService) {
    this.COMMENTS = databaseService.get().comments;
  }

  addComment(cardId: number, text: string): void {
    const newComment: Comment = {
      id: this.COMMENTS.length + 1,
      cardId,
      text,
    };

    this.databaseService.add('comments', newComment);
  }

  getCommentById(commentId: number): Observable<Comment> {
    let comment = this.COMMENTS.find((el) => el.id === commentId);

    if (!comment) {
      comment = {
        cardId: -1,
        id: -1,
        text: '',
      };
    }

    return of(comment);
  }

  getComments(cardId: number): Observable<Comment[]> {
    const comments = of(this.COMMENTS.filter((el) => el.cardId === cardId));

    return comments;
  }
}

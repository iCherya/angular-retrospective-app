import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { Card } from '../interfaces';
import { DatabaseService } from './database.service';

@Injectable({
  providedIn: 'root',
})
export class CardService {
  CARDS: Card[];

  constructor(private databaseService: DatabaseService) {
    this.CARDS = databaseService.get().cards;
  }

  addCard(text: string, boardId: number): void {
    const newCard: Card = {
      id: this.CARDS.length + 1,
      boardId,
      text,
      rating: 0,
    };

    this.databaseService.add('cards', newCard);
  }

  getCardById(cardId: number): Observable<Card> {
    let card = this.CARDS.find((el) => el.id === cardId);

    if (!card) {
      card = {
        boardId: -1,
        id: -1,
        text: '',
        rating: 0,
      };
    }

    return of(card);
  }

  updateCardRating(cardId: number, value: number): void {
    this.getCardById(cardId).subscribe((card) => {
      card.rating += value;
    });

    this.databaseService.save();
  }

  updateCardCategory(cardId: number, newBoardId: number): void {
    this.getCardById(cardId).subscribe((card) => {
      card.boardId = newBoardId;
    });

    this.databaseService.save();
  }

  getCards(boardId: number): Observable<Card[]> {
    const cards = of(this.CARDS.filter((el) => el.boardId === boardId));

    return cards;
  }
}

export const DATABASE = {
  boards: [
    { id: 1, text: 'Good things' },
    { id: 2, text: 'Bad things' },
    { id: 3, text: 'Action items' },
  ],
  cards: [
    { boardId: 1, id: 1, text: 'Text 1', rating: 1 },
    { boardId: 1, id: 2, text: 'Text 2', rating: 2 },
    { boardId: 1, id: 3, text: 'Text 3', rating: 3 },
    { boardId: 2, id: 4, text: 'Text 4', rating: 4 },
    { boardId: 2, id: 5, text: 'Text 5', rating: 5 },
    { boardId: 3, id: 6, text: 'Text 6', rating: 6 },
  ],
  comments: [
    { cardId: 1, id: 1, text: 'go quasi quasi est est' },
    { cardId: 2, id: 2, text: 'quasi quod quasi at' },
    { cardId: 2, id: 3, text: 'quia molestiae' },
    { cardId: 4, id: 4, text: 'like' },
    { cardId: 4, id: 5, text: 'harum non quasi et ratione' },
  ],
};
